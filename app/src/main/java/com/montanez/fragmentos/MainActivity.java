package com.montanez.fragmentos;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.nfc.Tag;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.FrameLayout;

public class MainActivity extends AppCompatActivity {
    Button btnCarga1, btnCarga2;
    FrameLayout miMarco;
    Fragment f1, f2;
    FragmentManager miAdministrador;
    FragmentTransaction miFragmentTranstion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        miMarco = findViewById(R.id.framelaout);
        btnCarga1 = findViewById(R.id.btnCarga1);
        btnCarga2 = findViewById(R.id.btnCarga2);
        f1 = new Fragmento1();
        f2 = new Fragmento2();


        //Se requiere un FragmentManager
        //Se requiere un gestor de transacciones FragmentTransactions

        miAdministrador = getSupportFragmentManager();
        try {
            miFragmentTranstion = miAdministrador.beginTransaction();
            miFragmentTranstion.add(R.id.framelaout, f1);
            miFragmentTranstion.commit();

            btnCarga1.setOnClickListener( (v) -> {
                miFragmentTranstion = miAdministrador.beginTransaction();
                miFragmentTranstion.replace(R.id.framelaout, f1);
                miFragmentTranstion.commit();
            });

            btnCarga2.setOnClickListener( (v) -> {
                miFragmentTranstion = miAdministrador.beginTransaction();
                miFragmentTranstion.replace(R.id.framelaout, f2);
                miFragmentTranstion.commit();
            });

        }catch ( Exception error){
            //.d(Tag, error.getMessage());
        };


        




    }
}